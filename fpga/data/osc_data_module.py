import sys
import numpy
import visa	
import decimal

### edit these lines they are the only thing that cannot be pulled from the spectrum analyzer###
file_name = 'mackenzie_20171203_coincidence_test.txt' #names file format is name, date, wtf ur doing. do it like that and ur life will be easier. 
notes = 'testing complete module. two square wave inputs of 1kHz are used, and two outputs monitored- one of the xor gate, and one of the and gate, which has a constant 3.3 volts on one of the gates and is always on.'
lsr_pwr = str(1000) #put laser power in uW here
trans = str(.9) #put transmission percent here
###

rm = visa.ResourceManager() #produces list of devices attached to computer
device = ',' .join(rm.list_resources()) #assumes that the only device plugged in to the computer is the one that we wanan use which is. some weak ass programming. 
device = device.split(',')
osc = rm.open_resource(device[0], write_termination = '\n', read_termination = '\n') #osc is short for oscilloscope. ur welcome
osc.timeout = 10000 
print(osc.query('*IDN?')) #makes sure that we are connected to an actual device cause if we arent none of this garbage works anyway 

osc.write('init:cont OFF') #turns it off so we can pull a waveform before it changes, so its like. conistent with itself. 
print(osc.query('*OPC?')) #checks to make sure previous operation finished (prints a 1 if it did, 0 if not). this is useful for finding where things went wrong

osc.write(':WAV:POIN:MODE RAW')
qresult1 = osc.query(':WAV:POIN:MODE?')
print('Waveform points mode: %s' % qresult1)

osc.write(':WAVE:POIN 10240')
qresult2 = osc.query(':WAV:POIN?')
print('Waveform points available: %s' % qresult2)

osc.write(':WAV:SOUR CHAN1')
qresult3 = osc.query(':WAV:SOUR?')
print('Waveform source: %s' % qresult3)

osc.write(':WAV:FORM ascii')
qresult4 = osc.query(':WAV:FORM?')
print('Waveform format: %s' % qresult4)

# Get numeric values for later calculations.
x_increment = float(osc.query(':WAVeform:XINCrement?'))
x_origin = float(osc.query(':WAVeform:XORigin?'))
y_increment = float(osc.query(':WAVeform:YINCrement?'))
y_origin = float(osc.query(':WAVeform:YORigin?'))
y_reference = float(osc.query(':WAVeform:YREFerence?'))

# Get the waveform data.
sData = osc.query(':WAVeform:DATA?')
print(osc.query('*OPC?'))

a = len(sData)
sData = sData[10:a]	
sData = sData.split(',')

Data=[]
for i in sData:
	i = str(i)
	for char in '\'b,':
		i = i.replace(char, '')
	Data.append(i)

myData = []
for x in Data:
	y = float(x)
	myData.append(y)
tot_points = len(myData)

wave_data = file_name[:-4] + '_waveform_1.txt'
f = open(wave_data, 'w')
for x in myData:				#added this loop so it lists properly & interacts w/ mathematica
	f.write(str(x) + '\n')
f.close()


stop = (x_increment*int(qresult2)) + x_origin
start = (x_origin)
time = numpy.linspace(float(start), float(stop), float(tot_points))

time_data = file_name[:-4] + '_timedata_a.txt'
f = open(time_data, 'w')
for x in time:					#same loop as before
	f.write(str(x) +'\n')
f.close()

osc.write(':WAV:SOUR CHAN2')
qresult3 = osc.query(':WAV:SOUR?')
print('Waveform source: %s' % qresult3)

osc.write(':WAV:FORM ascii')
qresult4 = osc.query(':WAV:FORM?')
print('Waveform format: %s' % qresult4)

# Get numeric values for later calculations.
x_increment = float(osc.query(':WAVeform:XINCrement?'))
x_origin = float(osc.query(':WAVeform:XORigin?'))
y_increment = float(osc.query(':WAVeform:YINCrement?'))
y_origin = float(osc.query(':WAVeform:YORigin?'))
y_reference = float(osc.query(':WAVeform:YREFerence?'))

# Get the waveform data.
sData = osc.query(':WAVeform:DATA?')
print(osc.query('*OPC?'))

a = len(sData)
sData = sData[10:a]	
sData = sData.split(',')

Data=[]
for i in sData:
	i = str(i)
	for char in '\'b,':
		i = i.replace(char, '')
	Data.append(i)

myData = []
for x in Data:
	y = float(x)
	myData.append(y)
tot_points = len(myData)

wave_data = file_name[:-4] + '_waveform_2.txt'
f = open(wave_data, 'w')
for x in myData:				#added this loop so it lists properly & interacts w/ mathematica
	f.write(str(x) + '\n')
f.close()


stop = (x_increment*int(qresult2)) + x_origin
start = (x_origin)
time = numpy.linspace(float(start), float(stop), float(tot_points))

time_data = file_name[:-4] + '_timedata_b.txt'
f = open(time_data, 'w')
for x in time:					#same loop as before
	f.write(str(x) +'\n')
f.close()

osc.write(':WAV:SOUR CHAN3')
qresult3 = osc.query(':WAV:SOUR?')
print('Waveform source: %s' % qresult3)

osc.write(':WAV:FORM ascii')
qresult4 = osc.query(':WAV:FORM?')
print('Waveform format: %s' % qresult4)

# Get numeric values for later calculations.
x_increment = float(osc.query(':WAVeform:XINCrement?'))
x_origin = float(osc.query(':WAVeform:XORigin?'))
y_increment = float(osc.query(':WAVeform:YINCrement?'))
y_origin = float(osc.query(':WAVeform:YORigin?'))
y_reference = float(osc.query(':WAVeform:YREFerence?'))

# Get the waveform data.
sData = osc.query(':WAVeform:DATA?')
print(osc.query('*OPC?'))

a = len(sData)
sData = sData[10:a]	
sData = sData.split(',')

Data=[]
for i in sData:
	i = str(i)
	for char in '\'b,':
		i = i.replace(char, '')
	Data.append(i)

myData = []
for x in Data:
	y = float(x)
	myData.append(y)
tot_points = len(myData)

wave_data = file_name[:-4] + '_waveform_3.txt'
f = open(wave_data, 'w')
for x in myData:				#added this loop so it lists properly & interacts w/ mathematica
	f.write(str(x) + '\n')
f.close()


stop = (x_increment*int(qresult2)) + x_origin
start = (x_origin)
time = numpy.linspace(float(start), float(stop), float(tot_points))

time_data = file_name[:-4] + '_timedata_c.txt'
f = open(time_data, 'w')
for x in time:					#same loop as before
	f.write(str(x) +'\n')
f.close()

osc.write(':WAV:SOUR CHAN4')
qresult3 = osc.query(':WAV:SOUR?')
print('Waveform source: %s' % qresult3)

osc.write(':WAV:FORM ascii')
qresult4 = osc.query(':WAV:FORM?')
print('Waveform format: %s' % qresult4)

# Get numeric values for later calculations.
x_increment = float(osc.query(':WAVeform:XINCrement?'))
x_origin = float(osc.query(':WAVeform:XORigin?'))
y_increment = float(osc.query(':WAVeform:YINCrement?'))
y_origin = float(osc.query(':WAVeform:YORigin?'))
y_reference = float(osc.query(':WAVeform:YREFerence?'))

# Get the waveform data.
sData = osc.query(':WAVeform:DATA?')
print(osc.query('*OPC?'))

a = len(sData)
sData = sData[10:a]	
sData = sData.split(',')

Data=[]
for i in sData:
	i = str(i)
	for char in '\'b,':
		i = i.replace(char, '')
	Data.append(i)

myData = []
for x in Data:
	y = float(x)
	myData.append(y)
tot_points = len(myData)

wave_data = file_name[:-4] + '_waveform_4.txt'
f = open(wave_data, 'w')
for x in myData:				#added this loop so it lists properly & interacts w/ mathematica
	f.write(str(x) + '\n')
f.close()


stop = (x_increment*int(qresult2)) + x_origin
start = (x_origin)
time = numpy.linspace(float(start), float(stop), float(tot_points))

time_data = file_name[:-4] + '_timedata_d.txt'
f = open(time_data, 'w')
for x in time:					#same loop as before
	f.write(str(x) +'\n')
f.close()

f = open(file_name, 'w')
f.write(notes)
f.close()