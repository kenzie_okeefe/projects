module mojo_top(
    // 50MHz clock input
    input clk,
    // Input from reset button (active low)
    input rst_n,
    // cclk input from AVR, high when AVR is ready
    input cclk,
    // Outputs to the 8 onboard LEDs
    output[7:0]led,
    // AVR SPI connections
    output spi_miso,
    input spi_ss,
    input spi_mosi,
    input spi_sck,
    // AVR ADC channel select
    output [3:0] spi_channel,
    // Serial connections
    input avr_tx, // AVR Tx => FPGA Rx
    output avr_rx, // AVR Rx => FPGA Tx
    input avr_rx_busy, // AVR Rx buffer full
    
    input active,
    input input_1,
    input input_2, 
    
    output out_xor,
    output out_and
    
    );

wire rst = ~rst_n; // make reset active high

//lets us use the output in the final and gate
wire xor_output = out_xor; //same as previous wire dec
wire input_a = input_1;
wire input_b = input_2; 
wire activated = active; 


// these signals should be high-z when not used
assign spi_miso = 1'bz;
assign avr_rx = 1'bz;
assign spi_channel = 4'bzzzz;

// probably will keep this here to check that the device is on, might hoot it up to an output to be /extra/ sure
assign led = 8'b11111111;

// start of coincidence detection

assign out_xor = input_a ^ input_b; //xors the inputs from psm 1 and 2 (our coincidence signals)
assign out_and = xor_output & activated; //final and gate, tells us if we had a signal at only one detector or not at the same time as we had a hit at the other detector

endmodule