import sys
import numpy
import pylab
import matplotlib
import matplotlib.pyplot as plt
import visa	
import decimal
#visa.log_to_screen()


### edit these lines they are the only thing that cannot be pulled from the spectrum analyzer###
file_name = 'mackenzie_20170629_50000x2_100uW.txt' #names file 
notes = 'test'
lsr_pwr = str(0) #put laser power in uW here
trans = str(.9) #put transmission percent here
###

rm = visa.ResourceManager() #produces list of devices attached to computer
device = ',' .join(rm.list_resources()) #assumes that the only device plugged in is the spectrum analyzer and pulls it out of the list as a sting
spec_analyzer = rm.open_resource(device, write_termination = '\n', read_termination = '\n') 
spec_analyzer.timeout = 10000 
print(spec_analyzer.query('*IDN?')) #makes sure that we are connected to an actual device

#might implement check here? make device selection more elegant. 

start_freq = spec_analyzer.query('SENS:FREQ:STAR?')
stop_freq = spec_analyzer.query('SENS:FREQ:STOP?')
rbw = spec_analyzer.query('SENS:BAND:RES?')
vbw = spec_analyzer.query('SENS:BAND:VID?')
swt = spec_analyzer.query('SENS:SWE:TIME?')
input_attn = spec_analyzer.query('SENS:POW:ATT?')


f = open(file_name, 'w') #cretes & opens file
f.write(notes)
f.write('\n')
f.write('start frequency=' + start_freq + ' Hz')
f.write('\n')
f.write('stop frequency=' + stop_freq + ' Hz')
f.write('\n')
f.write('RBW=' + rbw + ' Hz')
f.write('\n')
f.write('VBW=' + vbw + ' Hz')
f.write('\n')
f.write('SWT=' + swt + ' s')
f.write('\n')
f.write('laser power=' + lsr_pwr + ' uW')
f.write('\n')
f.write('transmission=' + trans)
f.write('\n')
f.write('input attenuation=' + input_attn + ' dB')
#yes i know this isnt efficient
f.close() 

spec_analyzer.write('init:cont OFF') #turns data collection off so that values can be taken
print(spec_analyzer.query('*OPC?'))

spec_analyzer.write('TRAC? TRACE1') #pulls data off of the analyzer
rawData = spec_analyzer.read_raw() 
print(spec_analyzer.query('*OPC?'))
a = len(rawData)
#print(a)

rawData = rawData.split()
rawData = rawData[1:a]	
# print(rawData)
# print(len(rawData))

Data=[]
for i in rawData:
	i = str(i)
	for char in '\'b,':
		i = i.replace(char, '')
	Data.append(i)
# print(Data)
# print(len(Data))

myData = []
for x in Data:
	y = float(x)
	myData.append(y)
#print(myData)
#print(len(myData))
tot_points = len(myData)

freq = numpy.arange(int(start_freq), int(stop_freq) + (int(stop_freq)/tot_points), (int(stop_freq)/tot_points)) 
freq = freq[1:-1] 

noise_data = file_name[:-4] + '_noisedata.txt'
f = open(noise_data, 'w')
for x in myData:				#added this loop so it lists properly & interacts w/ mathematica
	f.write(str(x), '\n')
#f.write(str(myData)) #need to fix this- make it acrually output a list. not hard to do 
f.close()

freq_data = file_name[:-4] + '_freqdata.txt'
f = open(freq_data, 'w')
for x in freq:					#same loop as before
	f.write(str(x), '\n')
#f.write(str(freq))
f.close()

plt.plot(freq, myData)
plt.savefig(file_name[:-4] + '_graph.png')
#plt.show()

spec_analyzer.write('init:cont ON') #turns the spectrum analyzer back on
print(spec_analyzer.query('*OPC?'))








